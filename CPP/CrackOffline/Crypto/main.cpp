#include <iostream>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <cryptopp/sha.h>
#include <cryptopp/cryptlib.h>
#include <list>
#include <chrono>

const char begchar = 32;
const char endchar = 127;

std::vector<std::string> initVector(std::vector<std::string> VFhash){
    char i (begchar);
    unsigned y (0);
    VFhash.resize(endchar-begchar);
    while (i != endchar){
        std::string str (1,i);
        VFhash[y] = str;
        i+=1;
        y+=1;
    }
    return VFhash;
}

std::vector<std::string> expand(std::vector<std::string> & VFhash, size_t& beg){
    if(VFhash.size()== 0)
        VFhash = initVector(VFhash);
    else{
        unsigned size (VFhash.size());
        for(unsigned i(beg); i < size; ++i){
            for(char c(begchar); c < endchar; ++c){
                VFhash.push_back(VFhash[i]+c);
            }
        }
        if(beg == 0)
            beg += endchar - begchar;
        else
            beg += beg*beg;
    }
    return VFhash;
}


std::vector<std::string> genereMsg(int & a){
    std::vector<std::string> VFhash;
    int i=0;
    size_t beg(0);
    while(i < a){
        VFhash = expand(VFhash, beg);
        i+=1;
    }
    return VFhash;
}


int main(){
    std::string message = "hash"; // doit être non vide
    std::string Fhash;
    std::string result;
    std::vector<std::string> VFhash;
    int a = 1;
    CryptoPP::SHA256 hashSHA256;
    std::string hash;

    hashSHA256.Update((const byte*)message.data(), message.size());
    hash.resize(hashSHA256.DigestSize());
    hashSHA256.Final((byte*)&hash[0]);
    hashSHA256.Restart();

    std::cout << "debut" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    while(hash != Fhash){
        VFhash = genereMsg(a);
        unsigned i = 0;
        while (hash != Fhash && i < VFhash.size()){
            Fhash.clear();
            hashSHA256.Update((const byte*)VFhash[i].data(), VFhash[i].size());
            Fhash.resize(hashSHA256.DigestSize());
            hashSHA256.Final((byte*)&Fhash[0]);
            hashSHA256.Restart();

            if(hash == Fhash)
                result = VFhash[i];
            i+=1;
        }
        a += 1;
    }

    auto stop = std::chrono::high_resolution_clock::now();

    std::cout << result << std::endl;
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

    std::cout << "durée : " << duration.count() << " millisecondes" << std::endl;
    return 0;
}
