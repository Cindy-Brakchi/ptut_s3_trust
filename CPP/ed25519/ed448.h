#ifndef ED448_H
#define ED448_H

#include "eddsa.hpp"
#include "eddsapublickey.hpp"
#include "ed448ellipticcurvepoint.h"
#include "publickeytypes.h"

namespace nsSignature
{
    class Ed448 : public EdDSA<456, nsKeys::Ed448PublicKey>
    {
    public:
        typedef nsEllipticCurve::Ed448EllipticCurvePoint Point;

        static constexpr size_t KEY_SIZE = 456;

        static const mpz_class _d;
        static const mpz_class _p;
        static const mpz_class _L;
        static const Point NEUTRAL_ELEMENT;
        static const Point _B;
        static const size_t SIGNATURE_SIZE;
        static const size_t SHAKE_OUTPUT_SIZE;

        virtual ~Ed448() override;

        virtual std::vector<CryptoPP::byte> sign(const std::string& m, const std::string &context = std::string()) const override;

        virtual size_t getSignatureSize() const override;

    private:
        static const std::string DOM4_ED448_CONSTANT_STRING;

        virtual Point decode(const std::vector<CryptoPP::byte> &vec) const override;
        std::vector<CryptoPP::byte> dom4(const std::vector<CryptoPP::byte>& context) const;
        virtual std::vector<CryptoPP::byte> hash(const std::vector<CryptoPP::byte>& data, const std::string& context = std::string()) const override;
    }; // Ed448
} // nsSignature

#endif // ED448_H
