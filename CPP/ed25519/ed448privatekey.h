#ifndef ED448PRIVATEKEY_H
#define ED448PRIVATEKEY_H

#include "eddsaprivatekey.hpp"
#include "publickeytypes.h"
#include "ed448.h"

namespace nsSignature::nsKeys
{
    class Ed448PrivateKey : public EdDSAPrivateKey<Ed448::KEY_SIZE, Ed448PublicKey>
    {
    public:
        Ed448PrivateKey(const std::string& keyString);
        virtual ~Ed448PrivateKey() override;

    private:
        virtual std::vector<CryptoPP::byte> hashKey() const override;
        virtual std::vector<CryptoPP::byte>& pruneData(std::vector<CryptoPP::byte>& data) const override;
    }; // Ed448PrivateKey
} // nsSignature

#endif // ED448PRIVATEKEY_H
