#ifndef RSAPRIVATEKEY_H
#define RSAPRIVATEKEY_H

#include <memory>
#include <gmpxx.h>
#include "rsakey.h"
#include "rsapublickey.h"

namespace nsCrypto
{
    class RSAPrivateKey : public RSAKey
    {
        mpz_class m_p;
        mpz_class m_q;

        RSAPrivateKey(unsigned long bits);
        RSAPrivateKey(const mpz_class& p, const mpz_class& q);

    public:
        //static factories -> les exceptions ne sont pas levées par les constructeurs
        static RSAPrivateKey createFromNSize(unsigned long bits); // may throw
        static RSAPrivateKey createFromTwoPrimes(const mpz_class& p, const mpz_class& q); // may throw

        RSAPublicKey getAssociatedPublicKey() const;
        const mpz_class& getP() const;
        const mpz_class& getQ() const;
        bool isPublicKeyValid(const std::shared_ptr<RSAPublicKey> &pk) const;
        void setP(const mpz_class& p);
        void setQ(const mpz_class& q);
    }; // class RSAPrivateKey
} // namespace nsCrypto

#endif // RSAPRIVATEKEY_H
