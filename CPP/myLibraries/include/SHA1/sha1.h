#ifndef SHA1_H
#define SHA1_H

#include <array>
#include <vector>
#include <fstream>

#include "HashFunction/hashfunction.h"

namespace nsHash
{
    class SHA1 : public HashFunction
    {
        static const std::array<uint32_t, 4> K;
        static const std::array<uint32_t, 5> registersInit;
        static constexpr unsigned blocksSize = 512;

        std::array<uint32_t, 5> m_hash;

    public:
        SHA1(const std::string & input = std::string());
        virtual void computeString();
        virtual void computeFile();
        const std::array<uint32_t, 5>& getResult() const;
        std::array<uint8_t, 20> getResultBytes() const;
	
	virtual std::string getResultAsString() const;

    private:
        void compute(std::istream &stream);
        uint32_t choice(uint32_t x, uint32_t y, uint32_t z) const;
        uint32_t majority(uint32_t x, uint32_t y, uint32_t z) const;
        uint32_t parity(uint32_t x, uint32_t y, uint32_t z) const;
        uint32_t Kval(uint32_t i) const;
        uint32_t ft(uint32_t t, uint32_t b, uint32_t c, uint32_t d) const;

        std::array<uint32_t, 16> toTab(const std::array<uint8_t, blocksSize / 8> &tab);
        void addPadding(std::array<uint8_t, 16*4> &tab, size_t pos, uint64_t l, bool add1);
        uint32_t RotateLeft(uint32_t val, unsigned short n);
        void resetHash();

    };
}


#endif // SHA1_H
