#include "RSAException/rsaException.h"
#include "RSAException/rsaExceptionCode.h"

nsCrypto::RSAException::RSAException(const std::string& str, const RSAExceptionCode& c)
    : m_str(str), m_c(c)
{}

/* virtual */ nsCrypto::RSAException::~RSAException() {}

/* virtual */ const char* nsCrypto::RSAException::what() const noexcept
{
    return m_str.c_str();
}

void nsCrypto::RSAException::display(std::ostream& os) const
{
    os << m_str << " " << m_c;
}
