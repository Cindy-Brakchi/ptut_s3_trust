#include "rsapublickey.h"
#include "numericutils.h"

nsCrypto::RSAPublicKey::RSAPublicKey(const mpz_class &e, const mpz_class &n)
    : RSAKey(e, n)
{}

void nsCrypto::RSAPublicKey::setN(const mpz_class& n)
{
    RSAKey::setN(n);
}
