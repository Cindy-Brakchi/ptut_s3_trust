\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{pgfplots}
\usepackage[a4paper, left=1.5cm, right=1.5cm, top=3cm, bottom=3cm]{geometry}

\pgfplotsset{compat=1.16}

\begin{document}
\section{Factorisation de n avec la méthode de Fermat}

\textit{Les durées d'exécutions sont des moyennes, nous avons effectué les tests avec la configuration suivante : } \\
\begin{itemize}
	\item Processeur : AMD Ryzen™ 7 3800XT 3.90GHz
	\item RAM : 16 Go DDR4 3200Mhz
\end{itemize}


\subsection{Factorisation de Fermat}

Soit \(n\) un entier naturel impair que l’on veut factoriser (nous pouvons donc l'utiliser pour le module de chiffrement RSA qui est impair). La méthode de Fermat repose sur l'idée suivante: puisque \(n\) est impair, alors on peut l'écrire comme une différence de deux carrés \(n = a^2 - b^2\) donc \(n = (a + b) (a - b)\). Le problème de la factorisation se ramène donc à un problème de soustraction. \\

On note toujours \(n\) le nombre à factoriser et on pose $q = E( \sqrt{n})$.
Puisque l’idée est de trouver $a$ et $b$ tels que $n = a^2 - b^2$ et que $a \geq q + 1$, alors on va choisir successivement $a = q + 1$ , $a = q + 2$, $a = q + 3$, … jusqu’à ce que $a^2 - n$ soit un carré.\\

\subsection{Exemples de Factorisation de Fermat}

Prenons n = 799 que nous voulons factoriser. nous avons donc $\sqrt{799}$ $\simeq$ 28,27 donc q = 28.
\begin{itemize}
    \item On essaye avec a = q + 1 = 29. On a $a^2$ - n = $29^2$ - 799 = 42. Ce n’est pas un carré.
	\item On essaye avec a = q + 2 = 30. On a $a^2$ - n = $30^2$ - 799 = 101. Ce n’est pas un carré.
	\item On essaye avec a = q + 3 = 31. On a $a^2$ - n = $31^2$ - 799 = 162. Ce n’est pas un carré.
	\item On essaye avec a = q + 4 = 32. On a $a^2$ - n = $32^2$ - 799 = 225 qui est un carré !\\
\end{itemize}

Comme 225 = $15^2$, on pose b = 15, et on a donc la relation $a^2$ - n = $b^2$. Ainsi, n = $a^2$ - $b^2$ = $32^2$ - $15^2$. On en déduit que n = (32 + 15) (32 - 15) c’est-à-dire n = $47 \times 17$.\\

Prenons n = 1300 que nous voulons factoriser. nous avons donc $\sqrt{1300}$ $\simeq$ 36.05 donc q = 36.
\begin{itemize}
    \item On essaye avec a = q + 1 = 37. On a $a^2$ - n = $37^2$ - 1300 = 69. Ce n’est pas un carré.
	\item On essaye avec a = q + 2 = 38. On a $a^2$ - n = $38^2$ - 1300 = 144 qui est un carré !\\
\end{itemize}

Comme 144 = $12^2$, on pose b = 12, et on a donc la relation $a^2$ - n = $b^2$. Ainsi, n = $a^2$ - $b^2$ = $38^2$ - $12^2$. On en déduit que n = (38 + 12) (38 - 12) c’est-à-dire n = $50 \times 26$.\\

Il y a donc autant d'itérations que nécessaire jusqu'à trouver un carré. Et plus la taille de n est élevée, plus le nombre d'itérations sera grand.

\subsection{Mesure du temps de factorisation}
\begin{center}
	\begin{tikzpicture}
	\begin{axis}
	[
	title={Temps de factorisation de n (RSA)},
	xlabel={$\mid p - q \mid$},
	ylabel={durée (en secondes)},
	xmin=0, xmax=310000000,
	ymin=0, ymax=32,
	xtick={0, 10079748, 37422048, 60641794, 87123364, 124652710, 159740520, 214648390, 260641432, 280955454, 310000000},
	ytick={0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32},
	legend pos=north west,
	ymajorgrids=true,
	grid style=dashed,
	scale=1.6
	]
	\addplot[smooth]
	[
	color=red,
	mark=*,
	]
	coordinates
	{
		(270, 0.0000813)(113988, 0.0000796)(10079748, 0.066)(37422048, 0.829)(60641794, 1.96)(87123364, 3.76)(124652710, 7.28)(159740520, 11.16)(184647318, 14.15)(214648390, 17.585)(234652764, 20.40)(260641432, 24.76)(280955454, 27.96)(305312938, 30.97)
	};


	\legend{Notre programme}
	\end{axis}
	\end{tikzpicture}
\end{center}

Le code utilisé pour ces mesures est disponible sur GitLab. En voici une partie :

\begin{minipage}{\textwidth}
	\begin{minted}{python}
	
	...
	
	nb = input('nb appels next_prime : ')
	nb = int(nb)
	if nb < 1 :
		raise ValueError('valeur strictemet positive attendue')
	
	p = input('p = ')
	p = int(p)
	if not is_prime(p) :
		raise ValueError('p et q doivent être premiers')
	
	startTime = time.time()
	q = p
	for i in range(nb) :
		q = next_prime(q)
	endTime = time.time()
	
	...
	
	n = p * q
	
	...
	
	startTime = time.time()
	result = factoFermat(n)
	endTime = time.time()
	
	...
	
	\end{minted}
\end{minipage}

L'idée est simple, à partir d'un nombre premier, nous appelons \verb|next_prime()| un nombre choisi de fois (cela a pour but de contrôler l'écart entre les deux nombres premier obtenus) puis nous factorisons le produit de ces deux nombres.

\begin{itemize}
	\item Si $\mid p - q \mid$ est petite, disons inférieure à $N^{1/4}$, alors la méthode de Fermat factorisera $n$ très facilement.\\
	\item Sinon si $\mid p - q \mid$ est beaucoup plus grande que $2N^{1/4}$, disons $\mid p - q \mid$ $>$ $2^{50}$ * $N^{1/4}$, alors la méthode de Fermat ne sera pas utilisable \\
\end{itemize}

\begin{center}
	\begin{tabular}{ |c|c|c|c|c|c| } 
		\hline
		p & 139 358 239 & 139 358 239 & 139 358 239 & 139 358 239 & 139 358 239 \\
		\hline
		q & 139 358 509 & 139 472 227 & 149 437 987 & 226 481 603 & 264 010 949 \\
		\hline
		$\mid p - q \mid$ & 270 & 113 988 & 10 079 748 & 87 123 364 & 124 652 710 \\
		\hline
		n =  (p * q) & 19.420x$10^{15}$ & 19.436x$10^{15}$ & 20 825x$10^{15}$ & 31 562x$10^{15}$ & 36 792x$10^{15}$ \\
		\hline
		Taille de n (bits) & 55 & 55 & 55 & 55 & 56 \\
		\hline
		Nbre de valeurs testées pour a & 1 & 12 & 87 980 & 5 262 731 & 9 871 923 \\
		\hline
		Itérations ( p + q - 2$\sqrt{N}$) & 1.3x$10^{-4}$ & 23.2 & 175 958 & 10 525 461 & 19 743 845 \\
		\hline
		$N^{1/4}$ & 11 805 & 11 807 & 12 013 & 13 329 & 13 849 \\
		\hline
		$2^{50}$ * $N^{1/4}$ & 1.33x$10^{19}$ & 1.33x$10^{19}$ & 1.35x$10^{19}$ & 1.50x$10^{19}$ & 1.56x$10^{19}$ \\
		\hline
		Temps & 8.13x$10^{-5}$ & 7.96x$10^{-5}$ & 0.066 & 3.76 & 7.28 \\
		\hline
	\end{tabular} \\
	Les temps sont en secondes\\
	
	
\end{center}


Grâce aux mesures réalisées, nous constatons en effet que lorsque la différence entre les deux nombres premiers p et q est très petite, la factorisation de Fermat fonctionne très facilement et rapidement, et plus la différence augmente, moins la factorisation est efficace, et plus elle met du temps. Cependant son évolution n'est pas linéaire.

\subsection{Evolution du nombre d'itération en fonction de n}

\begin{center}
	\begin{tikzpicture}
	\begin{axis}
	[
	title={itérations en fonction de la taille de N},
	xlabel={Valeur de n},
	ylabel={Nombre d'itérations},
	xmin=19420756403905651, xmax=70000000000000000,
	ymin=0, ymax=111500000,
	xtick={0, 20825414708024893, 24635789486234593, 27871652398821887, 31562077359977117, 36792100929358811, 48776258959099159, 54350911551497161, 69680371912493893},
	ytick={0, 5462166, 10525461, 19743845, 49140868, 70000000},
	legend pos=north west,
	ymajorgrids=true,
	grid style=dashed,
	scale=1.6
	]
	\addplot[smooth]
	[
	color=red,
	mark=*,
	]
	coordinates
	{
	(19420756403905651, 0.00013)(19436603944128253, 23.2)(20825414708024893, 175958)(24635789486234593, 2222680)(27871652398821887, 5462166)(31562077359977117, 10525461)(36792100929358811, 19743845)(48776258959099159, 47657563)(54350911551497161, 63101199)(69680371912493893, 111426427)
	};
	
	\addplot[smooth]
	[
	color=blue,
	mark=*,
	]
	coordinates
	{
		(19420756403905651, 1)(19436603944128253, 12)(20825414708024893, 87980)(31562077359977117, 5262731)(36792100929358811, 9871923)(48776258959099159, 23828782)(54350911551497161, 31550600)(69680371912493893, 55713214)
	};
	
	
	\legend{Estimation max, Notre programme}
	\end{axis}
	\end{tikzpicture}
\end{center}

Suite aux mesures réalisées, nous pouvons également constater que plus le produit des nombres premiers p et q (n) augmente, plus le nombre d'itérations nécessaires pour factoriser devient important.
	
\end{document}
