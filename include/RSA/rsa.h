#ifndef RSA_H
#define RSA_H

#include <memory>
#include "gmpxx.h"
#include "rsakey.h"

namespace nsCrypto
{
    class RSA
    {
        std::shared_ptr<RSAKey> m_key;
    public:
        RSA(const std::shared_ptr<RSAKey> &k = nullptr);
        std::shared_ptr<RSAKey> getKey() const;
        void setKey(const std::shared_ptr<RSAKey>& k);
        mpz_class compute(const mpz_class& data) const;
    }; // class RSA
} // namespace nsCrypto

#endif // RSA_H
